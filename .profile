#!/bin/bash

export CLICOLOR=1

export GOPATH=$HOME/go
export GOBIN=$GOPATH/bin

export PATH=$PATH:$GOBIN
export PATH=/opt/local/bin:/opt/local/sbin:$PATH

export CC=clang
export CXX=clang++
export MAKEFLAGS="${MAKEFLAGS} --no-print-directory"

if [[ "$TERM_PROGRAM" == "iTerm.app" ]];
then
  export TMOPTS="-2 -CC"
fi

