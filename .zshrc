export ZSH=$HOME/.oh-my-zsh

ZSH_THEME=${ZSH_THEME:-oxide}

CASE_SENSITIVE=true
DISABLE_AUTO_UPDATE=true

DEFAULT_USER="$USER"
POWERLEVEL9K_MULTILINE_FIRST_PROMPT_PREFIX=
POWERLEVEL9K_MULTILINE_LAST_PROMPT_PREFIX='# '
POWERLEVEL9K_PROMPT_ON_NEWLINE=true
POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS=(status command_execution_time time)

plugins=(git)

zstyle ':completion:*' special-dirs true

autoload bashcompinit
bashcompinit
autoload -U colors && colors
autoload -U promptinit
autoload -Uz compinit && compinit

source $ZSH/oh-my-zsh.sh
#LSCOLORS=Exfxcxdxbxegedabagacad

HISTSIZE=10000
SAVEHIST=10000

setopt nosharehistory

# disable globbing (fix [] annoyance)
# setopt noglob

# Enable prompt themes
setopt PROMPT_SUBST

export PROMPT_EOL_MARK=""

[[ -s ~/.alias ]] && source ~/.alias
[[ -s ~/.profile ]] && source ~/.profile

[[ -s /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh ]] && source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
