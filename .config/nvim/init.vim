" Install vim-plug if we don't already have it
if empty(glob('~/.config/nvim/autoload/plug.vim'))
    silent !curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs
        \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

syntax on
filetype off

call plug#begin('~/.config/nvim/plugged')

Plug 'airblade/vim-gitgutter'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'flazz/vim-colorschemes'
Plug 'jamessan/vim-gnupg'
Plug 'morhetz/gruvbox'
Plug 'tpope/vim-fugitive'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'vim-syntastic/syntastic'

call plug#end()

filetype plugin indent on

set t_Co=256
set termguicolors
set background=dark
let g:airline_theme='gruvbox'
let g:gruvbox_contrast_dark='medium'
let g:gruvbox_italic=1
colorscheme gruvbox

set undofile
set undodir=~/.vim/undo

set cursorline
set number
set relativenumber
set nowrap

set hlsearch
set incsearch

set noexpandtab
set tabstop=8
set listchars=tab:>-
set list
set backspace=indent,eol,start

set splitright

set completeopt-=preview

set clipboard=unnamedplus

set laststatus=2
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

set updatetime=0

let g:syntastic_mode_map={'mode': 'passive'}
let g:syntastic_always_populate_loc_list=1
let g:syntastic_auto_loc_list=1
let g:syntastic_check_on_open=1
let g:syntastic_check_on_wq=0

let g:syntastic_c_config_file='.syntastic_c_config'

let g:syntastic_python_python_exec='python3'
let g:syntastic_python_flake8_exec='python3'
let g:syntastic_python_flake8_args=['-m', 'flake8']
let g:syntastic_python_checkers=['python ', 'flake8']

let g:airline#extensions#branch#enabled=1
let g:airline#extensions#tagbar=1
let g:airline#extensions#whitespace#mixed_indent_algo=2

let g:deoplete#enable_at_startup=1

let python_highlight_all=1

command Q q

nnoremap <silent> <Leader>s :SyntasticToggleMode<CR>
nnoremap <silent> <C-]> g<C-]>
nnoremap <silent> <Leader>b :Gblame<CR>
nnoremap <silent> <Leader>h :noh<CR>
nnoremap <silent> <Leader>n :bnext<CR>
nnoremap <silent> <Leader>N :bNext<CR>
vnoremap <silent> <leader>p "_dP

highlight Comment cterm=italic

highlight TrailingWhitespace ctermbg=red guibg=#FF4400
match TrailingWhitespace /[\t ]\+$/

augroup type_c
	au!
	au FileType c
		\  setlocal colorcolumn=80
		\| let b:airline_whitespace_checks=[ 'indent', 'trailing', 'long' ]
augroup END

augroup gitcommit
	au!
	au FileType gitcommit
		\  setlocal spell
		\| setlocal colorcolumn=72
augroup END

augroup type_gocode
	au!
	au FileType go
		\ nnoremap <silent> <buffer> <Leader>gc :GoCoverageToggle<CR>
	au BufWritePost *.go
		\ silent GoTest
augroup END

augroup type_python
	au!
	au Filetype python
		\  setlocal expandtab
		\| setlocal tabstop=4
		\| setlocal softtabstop=4
		\| setlocal shiftwidth=4
		\| setlocal textwidth=100
		\| setlocal autoindent
		\| setlocal foldmethod=indent
		\| setlocal fileformat=unix
		\| setlocal colorcolumn=99
		\| setlocal encoding=utf-8
		\| let g:syntastic_mode_map={ 'mode': 'active' }
	au BufWritePre *.py execute ':Black'
augroup END

augroup type_diff
	au BufNewFile,BufRead *.patch highlight clear TrailingWhitespace
	au FileType diff highlight clear TrailingWhitespace
augroup END
